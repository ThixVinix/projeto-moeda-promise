var linha = document.querySelector("#idLinha");
var tableBody = document.querySelector("#idTableBody");

window.onload = function () {
    buscarMoedas();
}

function buscarMoedas() {
    fetch("https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/Moedas?$top=100&$format=json&$select=simbolo,nomeFormatado,tipoMoeda")
        .then(function (moedasDisponiveis) {
            return moedasDisponiveis.json();
        })
        .then(function (moedas) {
            for (let i = 0; i < moedas.value.length; i++) {
                criarLinha(moedas.value[i], i);
            }
        })
        .catch(function (erro) {
            alert("Erro ao buscar moedas: " + erro);
        })
        .finally(function () {
            console.log("Promise encerrada!");
        })
}

function criarLinha(moeda, i) {
    let novaLinha = document.createElement("tr");
    novaLinha.setAttribute("id", "idLinha" + (i + 1));

    let coluna1 = document.createElement("th");
    coluna1.setAttribute("class", "clNumeracaoLinha" + (i + 1));
    coluna1.textContent = (i + 1);

    let coluna2 = document.createElement("td");
    coluna2.textContent = moeda.simbolo

    let coluna3 = document.createElement("td");
    coluna3.textContent = moeda.nomeFormatado

    let coluna4 = document.createElement("td");
    coluna4.textContent = moeda.tipoMoeda

    novaLinha.appendChild(coluna1);
    novaLinha.appendChild(coluna2);
    novaLinha.appendChild(coluna3);
    novaLinha.appendChild(coluna4);

    tableBody.appendChild(novaLinha);
}